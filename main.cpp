#include <cassert>
#include <iostream>
#include "Circle.h"

int main(){
    Circle c(10);
    
    assert(c.area() == 314.159);
    
    std::cout << "All tests passed." << std::endl;
    
    return 0;
}