#ifndef CIRCLE_H_
#define CIRCLE_H_

class Circle {
public:
  Circle(const double radius);

  double area();

  double perimeter();

private:
  double radius;
};  

#endif